
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.logger.Log;
import com.j256.ormlite.logger.Log4jLog;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.StatementBuilder;
import com.j256.ormlite.stmt.query.Not;
import com.j256.ormlite.support.CompiledStatement;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.support.DatabaseConnection;
import com.j256.ormlite.support.DatabaseResults;
import org.apache.log4j.BasicConfigurator;
import org.eclipse.jetty.server.Authentication;
import org.json.JSONArray;
import org.json.JSONObject;
import spark.Request;
import spark.Response;
import spark.Route;
import spark.Spark;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.sun.xml.internal.ws.api.message.Packet.Status.Request;
import static com.sun.xml.internal.ws.api.message.Packet.Status.Response;
import static spark.Spark.get;
import static spark.Spark.port;
import static spark.Spark.post;

public class Driver {

    // todo: ensure that the connection source is not null when running the database.
    private String databaseUrl = "jdbc:mysql://localhost/simpleDB";
    private ConnectionSource connectionSource;

    // have the driver configure the properly setup for Spark.
    public Driver() {
        BasicConfigurator.configure();
        port(8082); // the port number that our API will be running on.

        try {
            this.connectionSource = new JdbcConnectionSource(databaseUrl);
            ((JdbcConnectionSource)connectionSource).setUsername("thenotoriousrog"); // Note: this would normally hidden from the team in a real setting
            ((JdbcConnectionSource)connectionSource).setPassword("Rh593961"); // Note: this would normally be hidden from the team in a real setting.
        } catch (SQLException ex) {
            ex.printStackTrace();
            System.out.println("Error attempting to connect to database");
        }


    }

    public String getUser(final String id) {
        try {
            System.out.println("initial test");
//            ConnectionSource connectionSource = new JdbcConnectionSource(databaseUrl);
//            ((JdbcConnectionSource)connectionSource).setUsername("thenotoriousrog"); // Note: this would normally hidden from the team in a real setting
//            ((JdbcConnectionSource)connectionSource).setPassword("Rh593961"); // Note: this would normally be hidden from the team in a real setting.

            final Dao<UserDB, String> userDao = DaoManager.createDao(connectionSource, UserDB.class);

            System.out.println("test");
            get("/users/:id", new Route() {
                public Object handle(spark.Request request, spark.Response response) throws Exception {

                    UserDB user = userDao.queryForId(id);

                    if(user != null) {
                        return "Username: " + user.getUsername() + " password: " + user.getPassword() + " email: " + user.getEmail() + " Id: " + user.getId();
                    }
                    else {
                        response.status(404); // not found
                        return "User not found :(";
                    }
                }
            });
        } catch (SQLException ex) {
            System.out.println("Failure test");
        }

        return "Error getting user";
    }

    private String getNoteJson(NoteDB note) {

        String jsonString = new JSONObject()
                .put("CreatorId", note.getCreatorId())
                .put("Subject", note.getSubject())
                .put("NoteText", note.getNoteText()).toString();

        return jsonString;

    }

    private JSONObject getNoteJsonObject(NoteDB note) {

        JSONObject json = new JSONObject()
                .put("CreatorId", note.getCreatorId())
                .put("Subject", note.getSubject())
                .put("NoteText", note.getNoteText());


        return json;

    }

    // creates a json array based on
    private JSONArray getJsonArrayForList(ArrayList<NoteDB> notes) {

        JSONArray jsonArray = new JSONArray();

        for(int i = 0; i < notes.size(); i++) {

            String jsonString = getNoteJson(notes.get(i));
            JSONObject json = getNoteJsonObject(notes.get(i));
            jsonArray.put(json);
        }

        return jsonArray;
    }

    public String getNote(final String creatorId) {
        try {
            System.out.println("initial test");

            final Dao<NoteDB, String> noteDao = DaoManager.createDao(connectionSource, NoteDB.class);

            System.out.println("test");
            get("/notes/:id", new Route() {
                public Object handle(spark.Request request, spark.Response response) throws Exception {

                    // todo: add the code to be able to get a list of notes and then build the json to reflect those note items and that will need to be passed back to the user.
                    NoteDB note = noteDao.queryForId(creatorId);

                    List<NoteDB> allNotes = noteDao.queryForAll();
                    ArrayList<NoteDB> notes = new ArrayList<NoteDB>(allNotes.size()); // all of the notes that we want that have the same creatorId

                    // iterate through all of our notes only selecting the one that has the same creator id
                    for(int i = 0; i < allNotes.size(); i++) {
                        if(note.getCreatorId() == allNotes.get(i).getCreatorId()) {
                            notes.add(allNotes.get(i));
                        }
                    }

                    // todo: create the json array for all of our notes.

                    System.out.println("THE SIZE OF THE LIST OF NOTES: " + notes.size());

                    if(note != null) {
                        JSONArray array = getJsonArrayForList(notes);
                        return array.toString();
                        //return getNoteJson(note);
                    }
                    else {
                        response.status(404); // not found
                        return "Note not found :(";
                    }
                }
            });
        } catch (SQLException ex) {
            System.out.println("Failure test");
        }

        return "Error getting note";
    }

    public String postNote() {

        post("/notes", new Route() {
            public Object handle(spark.Request request, spark.Response response)  {

                try {
                    System.out.println("Attempting to run the post now!!!!!");
                    String creatorId = request.queryParams("CreatorId");
                    String subject = request.queryParams("Subject");
                    String noteText = request.queryParams("NoteText");

                    System.out.println("CreatorId grabbed = " + creatorId);
                    System.out.println("subject grabbed = " + subject);
                    System.out.println("noteText grabbed = " + noteText);

                    NoteDB newNote = new NoteDB();
                    //newNote.setCreatorId(Integer.parseInt(creatorId));
                    newNote.setCreatorId(1);
                    newNote.setSubject(subject);
                    newNote.setNoteText(noteText);

//                    ConnectionSource connectionSource = new JdbcConnectionSource(databaseUrl);
//                    ((JdbcConnectionSource)connectionSource).setUsername("thenotoriousrog"); // Note: this would normally hidden from the team in a real setting
//                    ((JdbcConnectionSource)connectionSource).setPassword("Rh593961"); // Note: this would normally be hidden from the team in a real setting.

                    final Dao<NoteDB, String> noteDao = DaoManager.createDao(connectionSource, NoteDB.class);
                    noteDao.create(newNote);

                    response.status(201);
                    return "Note Created Successfully";
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                response.status(500);
                return "Note creation unsuccessful";
            }
        });

        return "Note creation unsuccessful";

    }

    public static void main(String[] args) {

        Driver driver = new Driver();
        String testUser = driver.getUser("1"); // get the user data here.
        String testPostNote = driver.postNote();
        String testNote = driver.getNote("1"); // get the note from the user's creator note i.e. user 1's notes.

        System.out.println("Response after posting a new note: " + testPostNote);

    }

}
