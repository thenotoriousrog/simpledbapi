
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

// This class is used to set and retrieve data for a notes item for the user.
@DatabaseTable(tableName = "notes")
public class NoteDB {

    @DatabaseField(generatedId = true)
    private int id;
    @DatabaseField
    private int creator_id;
    @DatabaseField
    private String subject;
    @DatabaseField
    private String messagetext;

    public NoteDB() {} // needed for ORMLite

    public int getId() {
        return this.id;
    }

    public int getCreatorId() {
        return this.creator_id;
    }

    public String getSubject() {
        return this.subject;
    }

    public String getNoteText() {
        return this.messagetext;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setCreatorId(int creatorId) {
        this.creator_id = creatorId;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public void setNoteText(String noteText) {
        this.messagetext = noteText;
    }
}
